module.exports = {
    labels: {
        in: 'body',
        isArray: true,
        notEmpty: true,
        errorMessage: 'labels should be an array and not empty',
    },
    'labels.*.sample': {
        in: 'body',
        isString: true,
        notEmpty: true,
        errorMessage: 'sample should be an string and not empty',
    },
    'labels.*.parameters': {
        in: 'body',
        isArray: true,
        errorMessage: 'parameters should be an array',
    },
    'labels.*.parameters.*': {
        in: 'body',
        isObject: true,
        notEmpty: true,
        errorMessage: 'elements of parameters should be an object and not empty',
    },
}
