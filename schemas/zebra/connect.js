module.exports = {
    host: {
        in: 'query',
        isIP: true,
        notEmpty: true,
        errorMessage: 'The host parameter must not be empty and be an IP address',
    },
    port: {
        in: 'query',
        isPort: true,
        notEmpty: true,
        errorMessage: 'The  port must be not empty and be an not negative integer',
    },
}
