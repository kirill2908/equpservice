const router = require('express').Router()
const controllers = require('../controllers')
const schemas = require('../schemas')

const { validate } = require('../middleware')

router.get('/system/ping', controllers.system.ping)

router.get('/zebra/connect', validate(schemas.zebra.connect), controllers.zebra.connect)
router.post('/zebra/print', validate(schemas.zebra.print), controllers.zebra.print)

module.exports = router
