module.exports = (layout, parameters) => {
    return layout.replace(/\%\(([^\)]+)?\)/g, (_, key) => parameters[key]).trim()
}
