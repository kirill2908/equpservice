module.exports = (layout, parameters) => {
    return layout.replace(/%(\d+)/g, (_, n) => parameters[+n - 1]).trim()
}
