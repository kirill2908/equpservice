const Net = require('net')
const { replaceObject } = require('../lib')

const port = 9100
const host = '192.168.53.10'
const timeout = 30000

layout = `        
^XA^CI28^PW500^FS
^RW24,26,A2^FS
^RS8,B13,100,1,E^FS
^RB96,8,3,3,24,58^FS
^RFW,E^FD52,0,5,1234567,%1,^FS
^FN3^RFR,H^FS
^HV3
^FWN^FO90, 15^AD,90,22^FDCompanyName^FS
^FT90,110^A0N,25,25^FH^FDUSER:%2,^FS
^FS^XZ`

parameters = ['p1', 'p2']

const arrayData = [
    { layout, parameters },
    { layout, parameters },
    { layout, parameters },
]
const client = new Net.Socket()

client.setTimeout(timeout)

client.connect({ port: port, host: host }, () => {
    console.log('TCP connection established with the server.')
})

client.on('ready', () => {
    let promises = arrayData.map((value) => {
        return new Promise((resolve) => {
            const fullyWritten = client.write(
                Buffer.from(replaceObject(value.layout, value.parameters)),
                'utf8',
                resolve
            )
            console.log('write: ' + fullyWritten)
        })
    })

    console.log('configuring promise handlers')

    Promise.all(promises)
        .then(() => console.log('Done'))
        .catch(() => console.log(reject))
        .finally(() => client.destroy())
})

client.on('error', (error) => {
    console.log(error)
})

client.on('close', () => {
    console.log('TCP connection is destroyed')
})

client.on('timeout', () => {
    console.log('socket timeout')
    client.destroy()
})
