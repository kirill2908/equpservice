const Net = require('net')
const { HttpError } = require('../../server')

module.exports = async (parameters) => {
    const { host, port, paramTimeout = 30000 } = parameters

    timeout = parseInt(paramTimeout)

    return new Promise((resolve, reject) => {
        if (!timeout || isNaN(timeout) || timeout < 0) {
            reject(new HttpError('Parameter timeout must be not empty and be an not negative integer', 400))
        }

        const client = new Net.Socket()
        client.setTimeout(timeout)

        client.connect({ port: port, host: host }, () => {
            console.log('TCP connection established with the server.')
        })

        client.on('ready', () => {
            client.destroy()

            process.env.socketHost = host
            process.env.socketPort = port
            process.env.socketTimeout = timeout

            console.log(process.env.socketHost, process.env.socketPort, process.env.socketTimeout)

            resolve(true)
        })

        client.on('error', (error) => {
            reject(error)
        })

        client.on('close', () => {
            console.log('TCP connection is destroyed')
        })

        client.on('timeout', () => {
            client.destroy()

            console.log('Connection socket timeout')
            reject(new HttpError('Connection socket timeout', 408))
        })
    })
}
