const Net = require('net')
const { replaceObject } = require('../../lib')
const { HttpError } = require('../../server')

module.exports = async (message) => {
    const port = process.env.socketPort
    const host = process.env.socketHost
    const timeout = process.env.socketTimeout || 30000

    const { labels } = message

    return new Promise((resolve, reject) => {
        if (!host) {
            reject(new HttpError('The host parameter must not be empty and be an IP address', 400))
        }

        if (!port) {
            reject(new HttpError('Parameter port must be not empty and be an not negative integer', 400))
        }

        const client = new Net.Socket()
        client.setTimeout(parseInt(timeout))

        client.connect({ port: port, host: host }, () => {
            console.log('TCP connection established with the server.')
        })

        client.on('ready', () => {
            console.log(labels)

            let promises = []

            labels.forEach((value) => {
                let promisesParameters = value.parameters.map((parameter) => {
                    return new Promise((resolve) => {
                        const fullyWritten = client.write(
                            Buffer.from(replaceObject(value.sample, parameter)),
                            'utf8',
                            resolve
                        )
                        console.log('write: ' + fullyWritten)
                    })
                })
                promises.push(...promisesParameters)
            })

            console.log(promises)
            console.log('configuring promise handlers')

            Promise.all(promises)
                .then(() => {
                    console.log('Done')
                    resolve(true)
                })
                .catch((error) => {
                    reject(error)
                })
                .finally(() => client.destroy())
        })

        client.on('error', (error) => {
            reject(error)
        })

        client.on('close', () => {
            console.log('TCP connection is destroyed')
        })

        client.on('timeout', () => {
            client.destroy()

            console.log('Connection socket timeout')
            reject(new HttpError('Connection socket timeout', 408))
        })
    })
}
