const { asyncHandler } = require('../../server')

exports.system = {
    ping: asyncHandler(require('./ping')),
}
