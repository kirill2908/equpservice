const { zebra } = require('./zebra')
const { system } = require('./system')

module.exports = { system, zebra }
