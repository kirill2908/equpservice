const { zebra } = require('../../models')

module.exports = async (req, res) => {
    result = await zebra.connect(req.query)
    res.sendStatus(result ? 200 : 404)
}
