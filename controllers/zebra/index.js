const { asyncHandler } = require('../../server')

exports.zebra = {
    print: asyncHandler(require('./print')),
    connect: asyncHandler(require('./connect')),
}
