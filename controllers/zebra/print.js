const { zebra } = require('../../models')

module.exports = async (req, res) => {
    result = await zebra.print(req.body)
    res.sendStatus(result ? 200 : 404)
}
