const { createServer } = require('./server')
const routes = require('./routes')

createServer(routes)
