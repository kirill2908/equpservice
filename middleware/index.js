const { validationResult, checkSchema } = require('express-validator')

const validate = (schema) => {
    const validator = checkSchema(schema)

    return async (req, res, next) => {
        await validator.run(req)

        const errors = validationResult(req)

        if (!errors.isEmpty()) {
            return res.status(400).json({ errors: errors.array() })
        }

        next()
    }
}

module.exports = {
    validate,
}
